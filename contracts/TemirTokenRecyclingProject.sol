pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/PausableToken.sol";

contract TemirTokenRecyclingProject is PausableToken, DetailedERC20 {
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    constructor (
        string _name, 
        string _symbol, 
        uint8 _decimals
    ) 
        DetailedERC20( _name, _symbol, _decimals )
        public
    {
        candidate = address(0);
        totalSupply_ = 1000*10**18;
        balances[msg.sender] = totalSupply_;
        emit Transfer(address(0), msg.sender, totalSupply_);
    }
    address public candidate;
    /**
    * @dev Throws if called by any account other than the owner.
    */
    modifier onlyCandidate() {
        require(msg.sender == candidate );
        _;
    }
    /**
    * @dev Allows the current owner to transfer control of the contract to a new Candidate.
    * @param _newCandidate The address to transfer ownership to.
    */
    function transferOwnership(address _newCandidate) public onlyOwner {
       candidate = _newCandidate;
    }
    /**
    * @dev Allows the current Candidate to accept control of the contract as a new Owner.
    */
    function confirmOwnership() public onlyCandidate {
       _transferOwnership(candidate);
    }
    /**
    * @dev Allows to transfer ownership to newOwner.
    * @param _newCandidate The address to transfer ownership to.
    */
    // function transferOwnershipDuringCrowdsale(address _newCandidate) public onlyOwner {
    //    _transferOwnership(_newCandidate);
    // }
    
    // -------------------------------------------------------------------
}