pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/emission/MintedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/CappedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol";

contract TemirTokenCrowdsale is Crowdsale, MintedCrowdsale, CappedCrowdsale, TimedCrowdsale {

    // investor contribution
    // 0.2 Ether minimum investor contribution 
    // 10.0 Ether maximum investor contribution
    uint256 public investorMinCap = 200000000000000000;     // in wei
    uint256 public investorHardCap = 10000000000000000000;   // in wei
    mapping( address => uint256 ) public contributions;      

    event test_value(uint256 value1);

    constructor(
        uint256 _rate, 
        address _wallet, 
        ERC20 _token,
        uint256 _cap,
        uint256 _openingTime,
        uint256 _closingTime
    )
        Crowdsale( _rate, _wallet, _token )
        CappedCrowdsale( _cap )
        TimedCrowdsale( _openingTime, _closingTime )
        public
    {

    }
    function _preValidatePurchase(
        address _beneficiary,
        uint256 _weiAmount
    )
        internal
    {
        super._preValidatePurchase( _beneficiary, _weiAmount );
        uint256 _existingContribution = contributions[_beneficiary];
        uint256 _newContribution = _existingContribution.add(_weiAmount);
        require(_newContribution >= investorMinCap && _newContribution <= investorHardCap);
        contributions[_beneficiary] = _newContribution;
    }

    function getUserContribution( address _beneficiary ) 
        public view returns (uint256)
    {
        return contributions[_beneficiary];
    }
}