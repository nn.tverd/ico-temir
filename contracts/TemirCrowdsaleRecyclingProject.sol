pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/CappedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/distribution/RefundableCrowdsale.sol";
// import "openzeppelin-solidity/contracts/crowdsale/emission/MintedCrowdsale.sol";

contract TemirCrowdsaleRecyclingProject is Crowdsale, CappedCrowdsale, RefundableCrowdsale {
// , CappedCrowdsale, RefundableCrowdsale

    constructor(
        uint256 _rate,         // How many token we are ready to give for 1 wei
        address _wallet,       // my wallet addres
        ERC20   _token,        // TemirTokenRecyclingProject address
        uint256 _cap,          // max amount of wei to be contributed
        uint256 _openingTime,  // Time of start saling
        uint256 _closingTime,  // Time of end saling
        uint256 _goal          // minimal amount of wei to be riesed
    )
        Crowdsale( _rate, _wallet, _token )
        CappedCrowdsale( _cap )
        TimedCrowdsale( _openingTime, _closingTime )
        RefundableCrowdsale(_goal)
        public
    {
        require(_goal<_cap);
    }

    
}
