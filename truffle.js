require("babel-register");
require('babel-polyfill');
const HDWalletProvider = require('truffle-hdwallet-provider');

require('dotenv').config();
var ropstenNet = `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`;
var mnemonic = process.env.MNEMONIC;

var g_mnem = 'image embody rose trust leaf crystal original pioneer talent sponsor evidence dilemma';
var g_host = 'http://127.0.0.1:8545';

// console.log(ropstenNet);
// console.log(mnemonic);

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // eslint-disable-line camelcase
    },
    ganache: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // eslint-disable-line camelcase
    },
    gan: {
      provider: function(){
        return new HDWalletProvider(
          g_mnem,
          g_host,
          1
        )
      },
      gasPrice: 25000000000,
      network_id: '*'
    },
    ropsten:{
      provider: function(){
        return new HDWalletProvider(
          mnemonic,
          ropstenNet,
          1
        )
      },
      gasPrice: 25000000000,
      network_id: 3
    }
  },
  solc:{
    optimazer:{
      enabled: true,
      runs:200
    }
  }
};
