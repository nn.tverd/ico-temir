#!/usr/bin/env bash

rm -rf flats/*
#./node_modules/.bin/truffle-flattener contracts/TemirToken.sol > flats/TemirToken_flat.sol
truffle-flattener contracts/TemirToken.sol > flats/TemirToken_flat.sol
truffle-flattener contracts/TemirTokenCrowdsale.sol > flats/TemirTokenCrowdsale_flat.sol
truffle-flattener contracts/TemirTokenRecyclingProject.sol > flats/TemirTokenRecyclingProject_flat.sol
truffle-flattener contracts/TemirCrowdsaleRecyclingProject.sol > flats/TemirCrowdsaleRecyclingProject_flat.sol