import EVMRevert from  "./helpers/EVMRevert";

const BigNumber = web3.BigNumber;
require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .should();

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();


const TemirTokenRecyclingProject = artifacts.require("TemirTokenRecyclingProject");

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract('TemirTokenRecyclingProject', ([investor1, investor2]) => {
// ([_, wallet, investor1, investor2])


    const _name     = "Temir Token Recycling Project";
    const _symbol   = "TTRP";
    const _decimals = 18;

    const _initOwner = investor1;
    const _candidate = investor2;
    console.log( "     ", "investor1 =", investor1 );
    console.log( "     ", "investor2 =", investor2 );
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    beforeEach( async function(){
        this.token = await TemirTokenRecyclingProject.new(_name, _symbol, _decimals);
        // console.log( this.token );
    });
    // -------------------------------------------------------------------
 
 
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('token attributes', function(){
        it('has the correct name', async function(){
            const name = await this.token.name();
            console.log( "     ", "TokenName =", name );
            name.should.equal(_name);
        });

        it('has the correct symbol', async function(){
            const symbol = await this.token.symbol();
            console.log( "     ", "TokenSymbol =", symbol );
            // assert.equal( name, _name );
            symbol.should.equal(_symbol);
        });

        it('has the correct _decimals', async function(){
            const decimals = await this.token.decimals();
            console.log( "     ", "TokenDecimals =", decimals.toNumber() );
            decimals.should.be.bignumber.equal(_decimals);
        });
    });
    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('changing owner', function(){
        it('candidate address at start is 0', async function(){
            const owner     = await this.token.owner();
            const candidate = await this.token.candidate();
            console.log( "     ", "Token owner =", owner );
            console.log( "     ", "Token candidate =", candidate );
            candidate.should.equal('0x0000000000000000000000000000000000000000');
        });

        it('owner is equal _initOwner before change', async function(){
            const owner     = await this.token.owner();
            console.log( "     ", "Token owner =", owner );
            console.log( "     ", "_initOwner =", _initOwner );
            owner.should.equal(_initOwner);
        });

        it('when candidate assing failed', async function(){
            await this.token.transferOwnership(_candidate, {from: _candidate}).should.be.rejectedWith(EVMRevert); // EVMRevert;
        });

        it('candidate assined succesfully', async function(){
            await this.token.transferOwnership(_candidate, {from: _initOwner});
            const candidate = await this.token.candidate();
            console.log( "     ", "Token candidate =", candidate );
            candidate.should.equal(_candidate);
        });

        it('when candidate confirm failed', async function(){
            const owner     = await this.token.owner();
            const candidate = await this.token.candidate();
            console.log( "     ", "Token owner =", owner );
            console.log( "     ", "Token candidate =", candidate );
            await this.token.confirmOwnership({from: _initOwner}).should.be.rejectedWith(EVMRevert); // EVMRevert;;
        });
        it(' candidate confirm successful', async function(){
            await this.token.transferOwnership(_candidate, {from: _initOwner});
            await this.token.confirmOwnership({from: _candidate});
            const owner     = await this.token.owner();
            const candidate = await this.token.candidate();
            console.log( "     ", "Token owner =", owner );
            console.log( "     ", "Token candidate =", candidate );
            owner.should.equal(_candidate);
        });
    });
    // -------------------------------------------------------------------
});