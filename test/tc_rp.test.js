const helpers = '../node_modules/openzeppelin-solidity/test/helpers';

// const helpers = '../node_modules/openzeppelin-solidity/test/helpers/';

const { assertRevert } = require(`${helpers}/assertRevert.js`);
const { ether } = require(`${helpers}/ether`);
const { ethGetBalance } = require(`${helpers}/web3`);
const { advanceBlock } = require(`${helpers}/advanceToBlock`);
const { expectThrow } = require(`${helpers}/expectThrow`);
const { EVMRevert } = require(`${helpers}/EVMRevert`);
const { increaseTimeTo, duration } = require(`${helpers}/increaseTime`);
const { latestTime } = require(`${helpers}/latestTime`);
// const { expectThrow } = require(`${helpers}/expectThrow`);


const BigNumber = web3.BigNumber;


const should = require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();

// const TemirTokenRecyclingProject     = artifacts.require("TemirTokenRecyclingProject");
// const TemirCrowdsaleRecyclingProject = artifacts.require("TemirCrowdsaleRecyclingProject");

const Crowdsale = artifacts.require('TemirCrowdsaleRecyclingProject');
const SimpleToken = artifacts.require('TemirTokenRecyclingProject');

contract('TemirCrowdsaleRecyclingProject', function ([_, investor, wallet, purchaser, anyone]) {
    // simple crowdsale
    const rate = new BigNumber(1);
    const value = ether(42);
    const tokenSupply = new BigNumber('1e21');
    const expectedTokenAmount = rate.mul(value);
    const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

    // capped crowdsale
    const cap = ether(100);
    const lessThanCap = ether(60);

    // refundable crowdsale
    const goal = ether(50);
    const lessThanGoal = ether(45);
    

    it('requires a non-null token', async function () {
        const openingTime = (await latestTime()) + duration.weeks(1);
        const closingTime = this.openingTime + duration.weeks(1);
        await assertRevert(
        Crowdsale.new(rate, wallet, ZERO_ADDRESS, cap, openingTime, closingTime, goal )
        );
    });

    before(async function () {
        // Advance to the next block to correctly read time in the solidity "now" function interpreted by ganache
        await advanceBlock();
    });

    context('with token deployed', async function () {
        beforeEach(async function () {

            this.openingTime = (await latestTime()) + duration.weeks(1);
            this.closingTime = this.openingTime + duration.weeks(1);
            this.afterClosingTime = this.closingTime + duration.seconds(1);
            this.preWalletBalance = await ethGetBalance(wallet);
    
            this.token = await SimpleToken.new('Temir Ttoken', 'TT', 18);
        });

        // refundable crowdsale
        it('rejects a goal of zero', async function () {
            await expectThrow(
                Crowdsale.new(
                    rate, wallet, this.token.address, cap, this.openingTime, this.closingTime,  0
                ),
                EVMRevert,
            );
        });

        // capped crowdsale 
        it('rejects a cap of zero', async function () {
            await expectThrow(
                Crowdsale.new(rate, wallet, this.token.address, 0, this.openingTime, this.closingTime, goal),
                EVMRevert,
            );
        });

        it('requires a non-zero rate', async function () {
        await assertRevert(
            Crowdsale.new(0, wallet, this.token.address, cap, this.openingTime, this.closingTime,  goal)
        );
        });

        it('requires a non-null wallet', async function () {
        await assertRevert(
            Crowdsale.new(rate, ZERO_ADDRESS, this.token.address, cap, this.openingTime, this.closingTime,  goal)
        );
        });

        context('with crowdsale deployed', async function () {
            beforeEach(async function () {
                this.crowdsale = await Crowdsale.new(rate, wallet, this.token.address, cap, this.openingTime, this.closingTime,  goal);
                // var bal = await this.token.balanceOf(_);
                // console.log( '            -- ', "balanceOf(_owner):", bal.toNumber() );
                // bal = await this.token.balanceOf(this.crowdsale.address);
                // console.log( '            -- ', "balanceOf(crowdsale.address):", bal.toNumber() );
                await this.token.transfer(this.crowdsale.address, tokenSupply);
                var bal = await this.token.balanceOf(_);
                console.log( '            -- -- ', "balanceOf(_owner):", bal.toNumber() );
                bal = await this.token.balanceOf(this.crowdsale.address);
                console.log( '            -- -- ', "balanceOf(crowdsale.address):", bal.toNumber() );
            });

            // refundable crowdsale
            context('before opening time', function () {
                it('denies refunds', async function () {
                    await expectThrow(this.crowdsale.claimRefund({from: investor}), EVMRevert);
                
                    // await expectThrow(this.crowdsale.claimRefund(investor), EVMRevert);
                });
            });

            context('after opening time', function () {
                beforeEach(async function () {
                    await increaseTimeTo(this.openingTime);
                });
          
                it('denies refunds', async function () {
                    await expectThrow(this.crowdsale.claimRefund({from:investor}), EVMRevert);
                    // await expectThrow(this.crowdsale.claimRefund(investor), EVMRevert);
                });
          
                context('with unreached goal', function () {
                    beforeEach(async function () {
                        await this.crowdsale.sendTransaction({ value: lessThanGoal, from: investor });
                    });
            
                    context('after closing time and finalization', function () {
                        beforeEach(async function () {
                            await increaseTimeTo(this.afterClosingTime);
                            var owner__ = await this.crowdsale.owner();
                            console.log('                  -- ', 'who is owner?', owner__, _);
                            // await this.crowdsale.finalize({ from: anyone });
                            await this.crowdsale.finalize({ from: _ });
                        });
            
                        it('refunds', async function () {
                            const pre = await ethGetBalance(investor);
                            await this.crowdsale.claimRefund({from:investor,  gasPrice: 0 });
                            const post = await ethGetBalance(investor);
                            post.minus(pre).should.be.bignumber.equal(lessThanGoal);
                        });
                    });
                });
          
                context('with reached goal', function () {
                    beforeEach(async function () {
                        await this.crowdsale.sendTransaction({ value: goal, from: investor });
                    });
            
                    context('after closing time and finalization', function () {
                        beforeEach(async function () {
                            await increaseTimeTo(this.afterClosingTime);
                            await this.crowdsale.finalize({ from: _ });
                            // await this.crowdsale.finalize({ from: anyone });
                        });
            
                        it('denies refunds', async function () {
                            await expectThrow(this.crowdsale.claimRefund({from:investor}), EVMRevert);
                        });
            
                        it('forwards funds to wallet', async function () {
                            const postWalletBalance = await ethGetBalance(wallet);
                            postWalletBalance.minus(this.preWalletBalance).should.be.bignumber.equal(goal);
                        });

                        
                    });
                });
            });

            // capped crowdsale
            describe('accepting payments', function () {
                it('should accept payments within cap', async function () {
                    await this.crowdsale.send(cap.minus(lessThanCap));
                    await this.crowdsale.send(lessThanCap);
                });
          
                it('should reject payments outside cap', async function () {
                    await this.crowdsale.send(cap);
                    await expectThrow(
                        this.crowdsale.send(1),
                        EVMRevert,
                    );
                });
          
                it('should reject payments that exceed cap', async function () {
                    await expectThrow(
                        this.crowdsale.send(cap.plus(1)),
                        EVMRevert,
                    );
                });
            });

            describe('ending', function () {
                it('should not reach cap if sent under cap', async function () {
                    await this.crowdsale.send(lessThanCap);
                    (await this.crowdsale.capReached()).should.equal(false);
                });
        
                it('should not reach cap if sent just under cap', async function () {
                    await this.crowdsale.send(cap.minus(1));
                    (await this.crowdsale.capReached()).should.equal(false);
                });
        
                it('should reach cap if cap sent', async function () {
                    await this.crowdsale.send(cap);
                    (await this.crowdsale.capReached()).should.equal(true);
                });
            });

            describe('accepting payments', function () {
                describe('bare payments', function () {
                    it('should accept payments', async function () {
                        await this.crowdsale.send(value, { from: purchaser });
                    });

                    it('reverts on zero-valued payments', async function () {
                        await assertRevert(
                        this.crowdsale.send(0, { from: purchaser })
                        );
                    });
                });

                describe('buyTokens', function () {
                    it('should accept payments', async function () {
                        await this.crowdsale.buyTokens(investor, { value: value, from: purchaser });
                    });

                    it('reverts on zero-valued payments', async function () {
                        await assertRevert(
                            this.crowdsale.buyTokens(investor, { value: 0, from: purchaser })
                        );
                    });

                    it('requires a non-null beneficiary', async function () {
                        await assertRevert(
                        this.crowdsale.buyTokens(ZERO_ADDRESS, { value: value, from: purchaser })
                        );
                    });
                });
            });

            describe('high-level purchase', function () {

                beforeEach(async function () {
                    await increaseTimeTo(this.openingTime);
                });

                it('should log purchase', async function () {
                    const { logs } = await this.crowdsale.sendTransaction({ value: value, from: investor });
                    const event = logs.find(e => e.event === 'TokenPurchase');
                    should.exist(event);
                    event.args.purchaser.should.equal(investor);
                    event.args.beneficiary.should.equal(investor);
                    event.args.value.should.be.bignumber.equal(value);
                    event.args.amount.should.be.bignumber.equal(expectedTokenAmount);
                });

                it('should assign tokens to sender', async function () {
                    await this.crowdsale.sendTransaction({ value: value, from: investor });
                    (await this.token.balanceOf(investor)).should.be.bignumber.equal(expectedTokenAmount);
                });

                it('should forward funds to wallet', async function () {
                    const pre = await ethGetBalance(wallet);
                    await this.crowdsale.sendTransaction({ value, from: investor });
                    const post = await ethGetBalance(wallet);
                    post.minus(pre).should.be.bignumber.equal(value);
                });
            });

            describe('low-level purchase', function () {
                
                beforeEach(async function () {
                    await increaseTimeTo(this.openingTime);
                });

                it('should log purchase', async function () {
                    const { logs } = await this.crowdsale.buyTokens(investor, { value: value, from: purchaser });
                    const event = logs.find(e => e.event === 'TokenPurchase');
                    should.exist(event);
                    event.args.purchaser.should.equal(purchaser);
                    event.args.beneficiary.should.equal(investor);
                    event.args.value.should.be.bignumber.equal(value);
                    event.args.amount.should.be.bignumber.equal(expectedTokenAmount);
                });

                it('should assign tokens to beneficiary', async function () {
                    await this.crowdsale.buyTokens(investor, { value, from: purchaser });
                    (await this.token.balanceOf(investor)).should.be.bignumber.equal(expectedTokenAmount);
                });

                it('should forward funds to wallet', async function () {
                    const pre = await ethGetBalance(wallet);
                    await this.crowdsale.buyTokens(investor, { value, from: purchaser });
                    const post = await ethGetBalance(wallet);
                    post.minus(pre).should.be.bignumber.equal(value);
                });
            });


            

        });
    });
});










/*
contract('TemirCrowdsaleRecyclingProject',  function ([_, investor, wallet, purchaser]) {
    // token data
    const _name = "Temir Token Recycling Project";
    const _symbol = "TT RP"
    const _decimals = 18;


    before(async function () {
        // Advance to the next block to correctly read time in the solidity "now" function interpreted by ganache
        await advanceBlock();
    });

    const rate = new BigNumber(1);
    const value = ether(42);
    const tokenSupply = new BigNumber('1e10');
    console.log(tokenSupply.toNumber());
    const expectedTokenAmount = rate.mul(value);
    const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

    // _token,
    const _cap= ether( 100 );
    
    // const _openingTime   = latestTime() + duration.weeks(1);
    // const _closingTime   = _openingTime + duration.weeks(1);
    const _goal          = ether(50);

    it('requires a non-null token', async function () {
        this.openingTime = (await latestTime()) + duration.weeks(1);
        this.closingTime = this.openingTime + duration.weeks(1);
        await assertRevert(
            TemirCrowdsaleRecyclingProject.new(
                rate, 
                wallet, 
                ZERO_ADDRESS,
                _cap,
                this.openingTime,
                this.closingTime,
                _goal
            )
        );
    });

    
    context('with token', async function () {
        beforeEach(async function () {
            this.openingTime = (await latestTime()) + duration.weeks(1);
            this.closingTime = this.openingTime + duration.weeks(1);
            this.afterClosingTime = this.closingTime + duration.seconds(1);


            this.token = await TemirTokenRecyclingProject.new( _name, _symbol, _decimals );
            const bal = await this.token.balanceOf(_)
            console.log( '        --', 'Balance of _ =', bal.toNumber() );
            const owner = await this.token.owner();
            console.log( '        --', 'owner =', owner );
            const totalSupply = await this.token.totalSupply();
            console.log( '        --', 'totalSupply =', totalSupply );
            
            
        });

        it('requires a non-zero rate', async function () {
            await assertRevert(
                TemirCrowdsaleRecyclingProject.new(
                    0, 
                    wallet, 
                    this.token.address,
                    _cap,
                    this.openingTime,
                    this.closingTime,
                    _goal
                )
            );
        });

        it('requires a non-null wallet', async function () {
            
            await assertRevert(
                TemirCrowdsaleRecyclingProject.new(
                    rate, 
                    ZERO_ADDRESS, 
                    this.token.address,
                    _cap,   
                    this.openingTime,
                    this.closingTime,
                    _goal
                )
            );
        });

        context('once deployed', async function () {
            beforeEach(async function () {
                this.crowdsale = await TemirCrowdsaleRecyclingProject.new(
                    rate, 
                    wallet, 
                    this.token.address,
                    _cap,   
                    this.openingTime,
                    this.closingTime,
                    _goal
                );
                console.log( '        --', 'crowdsale.address =', await this.crowdsale.address )
                await this.token.transfer( this.crowdsale.address, tokenSupply);
                // const newTime = _openingTime+1;
                // // console.log( this.openingTime, newTime );
                // await increaseTimeTo( newTime );
            });

            describe('accepting payments', function () {

                describe('bare payments', function () {
                    it('should accept payments', async function () {
                        await this.crowdsale.send(value, { from: purchaser });
                    });

                    it('reverts on zero-valued payments', async function () {
                        await assertRevert(
                          this.crowdsale.send(0, { from: purchaser })
                        );
                    });
                });

                describe('buyTokens', function () {
                    it('should accept payments', async function () {
                        await this.crowdsale.buyTokens(investor, { value: value, from: investor });
                    });
            
                    it('reverts on zero-valued payments', async function () {
                        await assertRevert(
                            this.crowdsale.buyTokens(investor, { value: 0, from: purchaser })
                        );
                    });
        
                    it('requires a non-null beneficiary', async function () {
                        await assertRevert(
                            this.crowdsale.buyTokens(ZERO_ADDRESS, { value: value, from: purchaser })
                        );
                    });
                });
            });
        });
    });
});

*/