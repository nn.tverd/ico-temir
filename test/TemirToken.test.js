const BigNumber = web3.BigNumber;

const TemirToken = artifacts.require("TemirToken");

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract('TemirToken', address =>{

    const _name     = "Temir Token";
    const _symbol   = "TMR";
    const _decimals = 18;

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    beforeEach( async function(){
        this.token = await TemirToken.new(_name, _symbol, _decimals);
    });
    // -------------------------------------------------------------------
 
 
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('token attributes', function(){
        it('has the correct name', async function(){
            const name = await this.token.name();
            console.log( "     ", "TokenName =", name );
            // assert.equal( name, _name );
            name.should.equal(_name);
        });

        it('has the correct symbol', async function(){
            const symbol = await this.token.symbol();
            console.log( "     ", "TokenSymbol =", symbol );
            // assert.equal( name, _name );
            symbol.should.equal(_symbol);
        });

        it('has the correct _decimals', async function(){
            const decimals = await this.token.decimals();
            console.log( "     ", "TokenDecimals =", decimals.toNumber() );
            decimals.should.be.bignumber.equal(_decimals);
        });
    });
    // -------------------------------------------------------------------
});