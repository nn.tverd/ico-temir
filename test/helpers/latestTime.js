// // returns the time of the latest mimed block in seconds
// export default function latestTime () {
//     return web3.eth.getBlock('latest').timestamp;
// }

const { ethGetBlock } = require('./web3');

// Returns the time of the last mined block in seconds
async function latestTime () {
  const block = await ethGetBlock('latest');
  return block.timestamp;
}

module.exports = {
  latestTime,
};