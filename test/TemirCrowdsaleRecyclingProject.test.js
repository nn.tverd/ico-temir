
// const { assertRevert } = require('./helpers/assertRevert');
// const { ether } = require('../helpers/ether');
const { ethGetBalance } = require('./helpers/web3');

import ether from  "./helpers/ether.js";
import EVMRevert from  "./helpers/EVMRevert.js";
import { increaseTimeTo, duration} from  "./helpers/increaseTime.js";
import latestTime from  "./helpers/latestTime.js";

const BigNumber = web3.BigNumber;

require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .should();


const should = require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();

const TemirTokenRecyclingProject     = artifacts.require("TemirTokenRecyclingProject");
const TemirCrowdsaleRecyclingProject = artifacts.require("TemirCrowdsaleRecyclingProject");

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract ('TemirCrowdsaleRecyclingProject', ([ _owner, _wallet, investor1, investor2]) => {
    
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    beforeEach( async function(){
        // Token Config
        this.name     = "Temir Token Recycling Project";
        this.symbol   = "TT RP";
        this.decimals = 18;
        this.tokenSupply = new BigNumber('1e20');
        // Token Deployment
        console.log('        ---','Token is ready to be deployed');
        this.token = await TemirTokenRecyclingProject.new(
            this.name, 
            this.symbol, 
            this.decimals
        );
        console.log('        ---','Token has been deployed');

        this.rate           = 220;  // token for 1 ether
        this.wallet         = _wallet;
        // this.token = "";
        this.cap            = ether(100);
        this.openingTime    = latestTime() + duration.weeks(1);
        this.closingTime    = this.openingTime + duration.weeks(1);
        this.goal           = ether(50); // 50 ether

        // Crowdsale Deployment
        console.log('        ---','Crowdsale is ready to be deployed');
        this.crowdsale = await TemirCrowdsaleRecyclingProject.new(
            this.rate,
            this.wallet,
            this.token.address,
            this.cap,
            this.openingTime,
            this.closingTime,
            this.goal
        );
        console.log('        ---','Crowdsale has been deployed');
        
        // transfer token ownership to crowd sale
        // const newTime = this.openingTime+1;
        // console.log( this.openingTime, newTime );
        // await increaseTimeTo( this.openingTime + 1 );

    });
    // -------------------------------------------------------------------
  
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('crowdsale', function(){
        it('tracks rate', async function () {
            const rate = await this.crowdsale.rate();
            console.log( '        --- ---   ', "Rate =", rate.toNumber() );
            rate.should.be.bignumber.equal(this.rate);
        });
        it('tracks wallet', async function () {
            const wallet = await this.crowdsale.wallet();
            console.log( '        --- ---   ', "Wallet =", wallet );
            wallet.should.equal(this.wallet);
        });
        it('tracks token', async function () {
            const token = await this.crowdsale.token();
            console.log( '        --- ---   ', "token =", token );
            token.should.equal(this.token.address);
        });
    });

    // -------------------------------------------------------------------


    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('capped tokens ', async function() {
        it('has right hard cap', async function () {
            const cap = await this.crowdsale.cap();
            console.log( '        --- ---   ', "cap =", cap );
            cap.should.be.bignumber.equal(this.cap)
        });    
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('timed crowdsale ', function() {
        it('is open', async function () {
            const isClosed = await this.crowdsale.hasClosed();
            isClosed.should.be.false;
        });    
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('accepting payment', function() {
        it('should accept payment', async function () {
            await this.token.transfer(this.crowdsale.address, this.tokenSupply, {from:_owner});    
            // console.log(await this.token.balanceOf(_owner) );
            try{
                const value = ether(1);
                // const purchaser = investor2;
                // await this.crowdsale.sendTransaction({value: value, from: investor1}).should.be.fulfilled;
                await this.crowdsale.buyTokens(investor1, {value:value, from: investor2}).should.be.fulfilled;
                console.log(await this.token.balanceOf(investor1) );
            }
            catch(e){
                console.log(e);
            }
        });    
    });

    // -------------------------------------------------------------------
    
});
