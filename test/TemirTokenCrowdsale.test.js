import ether from  "./dapphelpers/ether.js";
import EVMRevert from  "./dapphelpers/EVMRevert";
import { increaseTimeTo, duration} from  "./dapphelpers/increaseTime";
import latestTime from  "./dapphelpers/latestTime";

const BigNumber = web3.BigNumber;

require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .should();

const TemirToken            = artifacts.require("TemirToken");
const TemirTokenCrowdsale   = artifacts.require("TemirTokenCrowdsale");

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract ('TemirTokenCrowdsale', ([ , _wallet, investor1, investor2]) => {
    
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    beforeEach( async function(){
        // Token Config
        this.name     = "Temir Token";
        this.symbol   = "TMR";
        this.decimals = 18;
        // Token Deployment
        this.token = await TemirToken.new(
            this.name, 
            this.symbol, 
            this.decimals
        );
        // Crowdsale Config
        this.rate = 500;
        this.wallet = _wallet;
        // this.token = "";
        this.cap = ether(100);
        this.openingTime = latestTime() + duration.weeks(1);
        this.closingTime = this.openingTime + duration.weeks(1);

        // investor's contribution
        this.investorMinCap = ether(0.2);
        this.investorHardCap = ether(10);

        // Crowdsale Deployment
        this.crowdsale = await TemirTokenCrowdsale.new(
            this.rate,
            this.wallet,
            this.token.address,
            this.cap,
            this.openingTime,
            this.closingTime
        );
        // transfer token ownership to crowd sale
        await this.token.transferOwnership( this.crowdsale.address );

        // Advance time to crowdsale
        const newTime = this.openingTime+1;
        console.log( this.openingTime, newTime );
        await increaseTimeTo( this.openingTime + 1 );

    });
    // -------------------------------------------------------------------
  
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('crowdsale', function(){
        it('tracks rate', async function () {
            const rate = await this.crowdsale.rate();
            console.log( "     ", "Rate =", rate.toNumber() );
            rate.should.be.bignumber.equal(this.rate);
        });
        it('tracks wallet', async function () {
            const wallet = await this.crowdsale.wallet();
            console.log( "     ", "Wallet =", wallet );
            wallet.should.equal(this.wallet);
        });
        it('tracks token', async function () {
            const token = await this.crowdsale.token();
            console.log( "     ", "token =", token );
            token.should.equal(this.token.address);
        });
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('minting tokens ', function() {
        it('tokens are minted after purchase', async function () {
            const originalTotalSuplly = await this.token.totalSupply();
            const value = ether(1);
            await this.crowdsale.sendTransaction({value: value, from: investor1}).should.be.fulfilled;
            const newTotalSuplly = await this.token.totalSupply();
            console.log( "     ", "originalTotalSuplly =", originalTotalSuplly.toNumber() );
            console.log( "     ", "newTotalSuplly =", newTotalSuplly.toNumber() );
            // assert.isTrue( 0  > originalTotalSuplly );
            assert.isTrue( newTotalSuplly > originalTotalSuplly );
        });    
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('capped tokens ', async function() {
        it('has right hard cap', async function () {
            const cap = await this.crowdsale.cap();
            cap.should.be.bignumber.equal(this.cap)
        });    
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    describe('timed crowdsale ', function() {
        it('is open', async function () {
            const isClosed = await this.crowdsale.hasClosed();
            isClosed.should.be.false;
        });    
    });

    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('accepting payment', function() {
        it('should accept paument', async function () {
            const value = ether(1);
            const purchaser = investor2;
            await this.crowdsale.sendTransaction({value: value, from: investor1}).should.be.fulfilled;
            await this.crowdsale.buyTokens(investor2, {value:value, from: purchaser}).should.be.fulfilled;
        });    
    });

    // -------------------------------------------------------------------
    
    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('buyTokens()', function() {
        describe('when the contribution is the less than the minimum cap', function () {
            it('rejects the transaction', async function(){
                const value = this.investorMinCap.minus(1);
                await this.crowdsale.buyTokens(investor2, {value: value, from: investor2}).should.be.rejectedWith(EVMRevert); // EVMRevert
            });
        });    

        describe('when the investor already met min cap', function () {
            it('allow investor contribute bellow min cap', async function(){
                // first contribution
                const value1 = ether(1);
                await this.crowdsale.buyTokens(investor1, { value: value1, from: investor1});
                const value2 = this.investorMinCap.minus(1);
                await this.crowdsale.buyTokens(investor1, {value: value2, from: investor1}).should.be.fulfilled; // 
            });
        }); 
    });

    // -------------------------------------------------------------------


    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('when the total contribution exceed the investor hard cap', function() {
        it('reject the transaction', async function(){
            const value1 = ether(2);
            await this.crowdsale.buyTokens(investor1, {value: value1, from: investor1});
            const value2 = ether(9);
            await this.crowdsale.buyTokens(investor1, {value: value2, from: investor1}).should.be.rejectedWith(EVMRevert); // EVMRevert
        });
    });
    // -------------------------------------------------------------------

    // -------------------------------------------------------------------
    // 
    // -------------------------------------------------------------------
    
    describe('when the contribution is between caps', function() {
        it('success and update contribution', async function(){
            const value1 = ether(2);
            await this.crowdsale.buyTokens(investor1, {value: value1, from: investor1}).should.be.fulfilled;
            const contribution = await this.crowdsale.getUserContribution( investor1 );
            contribution.should.be.bignumber.equal(value1);
        });
    });
    // -------------------------------------------------------------------
});
