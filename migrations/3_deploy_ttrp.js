var TemirTokenRecyclingProject = artifacts.require("./TemirTokenRecyclingProject.sol");
var TemirCrowdsaleRecyclingProject = artifacts.require("./TemirCrowdsaleRecyclingProject.sol");

const ether = (n) => new web3.BigNumber(web3.toWei(n, 'ether'));

const duration = {
  seconds: function (val) { return val; },
  minutes: function (val) { return val * this.seconds(60); },
  hours:   function (val) { return val * this.minutes(60); },
  days:    function (val) { return val * this.hours(24); },
  weeks:   function (val) { return val * this.days(7); },
  years:   function (val) { return val * this.days(365); },
};

module.exports = async function(deployer, network, accounts) {
  const name = "Temir Token Recycling Project";
  const symbol = "TT RP";
  const decimals = 18;
  console.log( '     ', 'Deploying is coming' );
  console.log( '     ', network );
  await deployer.deploy( TemirTokenRecyclingProject, name, symbol, decimals );
  const tokenAddress = await TemirTokenRecyclingProject.deployed();
  // console.log("                     ", tokenAddres);


  const latestTime      = (new Date).getTime();
  const _rate           = 500;
  const _wallet         = accounts[0]; // TODO: Replace me
  // console.log( "                     ", '_wallet', _wallet )
  const _token          = tokenAddress.address;
  const _cap            = ether(100);
  const _openingTime    = latestTime + duration.minutes(1);
  const _closingTime    = _openingTime + duration.weeks(1);
  const _goal           = ether(50);

  console.log( "       ", "crowdsale ready to be deployed()" );

  await deployer.deploy(
    TemirCrowdsaleRecyclingProject,
    _rate,
    _wallet,
    _token,
    _cap,
    _openingTime,
    _closingTime,
    _goal
  );
  console.log( "       ", "Finished deploying crowdsale" );

  const CSDeployed = await TemirCrowdsaleRecyclingProject.deployed();

  // await console.log( 'CSDeployed', CSDeployed )

  return true;

};
