var TemirToken = artifacts.require("./TemirToken.sol");
// var TemirTokenCrowdsale = artifacts.require("./TemirTokenCrowdsale.sol");

// const ether = (n) => new web3.BigNumber(web3.toWei(n, 'ether'));

// const duration = {
//   seconds: function (val) { return val; },
//   minutes: function (val) { return val * this.seconds(60); },
//   hours: function (val) { return val * this.minutes(60); },
//   days: function (val) { return val * this.hours(24); },
//   weeks: function (val) { return val * this.days(7); },
//   years: function (val) { return val * this.days(365); },
// };

module.exports = async function(deployer, network, accounts) {
 /*
  
  const name = "Temir";
  const symbol = "TMR";
  const decimals = 18;
  await deployer.deploy( TemirToken, name, symbol, decimals );
// 0x6e0C73595D322352C6A81E3289F096b47A4f2E5C // token approved
// 0xc9523cf5f2be6a94f2b596c7324366efc1c60e41 // token crowdsale

  // return true;
  // const tokenAddress = '0x6e0C73595D322352C6A81E3289F096b47A4f2E5C';
  const tokenAddress = await TemirToken.deployed();
  console.log( 'tokenAddress :',tokenAddress.address )

  // return true;
  const latestTime = (new Date).getTime();

  const _rate           = 500;
  const _wallet         = accounts[0]; // TODO: Replace me
  console.log( '_wallet', _wallet )
  const _token          = tokenAddress;
  const _cap            = ether(100);
  const _openingTime    = latestTime + duration.minutes(1);
  const _closingTime    = _openingTime + duration.weeks(1);
  console.log( '_openingTime', _openingTime )
  console.log( '_closingTime', _closingTime )
  // const _goal           = ether(50);
  // const _foundersFund   = accounts[0]; // TODO: Replace me
  // const _foundationFund = accounts[0]; // TODO: Replace me
  // const _partnersFund   = accounts[0]; // TODO: Replace me
  // const _releaseTime    = _closingTime + duration.days(1);
  console.log( "ready to deploy crowdsale" )
  await deployer.deploy(
    TemirTokenCrowdsale,
    _rate,
    _wallet,
    _token,
    _cap,
    _openingTime,
    _closingTime
    // _goal,
    // _foundersFund,
    // _foundationFund,
    // _partnersFund,
    // _releaseTime
  );
  // uint256 _rate, 
  //       address _wallet, 
  //       ERC20 _token,
  //       uint256 _cap,
  //       uint256 _openingTime,
  //       uint256 _closingTime
  console.log( "Finished deploying crowdsale" );

  const CSDeployed = await TemirTokenCrowdsale.deployed();

  await console.log( 'CSDeployed', CSDeployed )
  
  //   */
  return true;

};
